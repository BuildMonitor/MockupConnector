Connector =
{
    FormData:
    {
        Name: { Type: "text", Title: "Name", Value: "Mockup Connector" },
        Text: { Type: "text", Title: "Text Config", Value: "" },
        Number: { Type: "number", Title: "Number Config", Value: "" },
        StringList: { Type: "stringList", Title: "StringList Config", Value: "" }
    },

    Index: 0,

    Shared: {
        Projects:
        {
            "Success": {
                "Identifier": "Success",
                "Name": "Project Success",
                "Status": "Success",
                "Reference": "feature/Success",
                "Username": "Michael Loeffl",
                "Avatar": "none",
                "Weather": 1.0,
                "ImportantRefStatus": "Success",
                "StartedAt": "2017-09-30T02:29:26+02:00",
                "FinishedAt": "2017-09-30T02:30:26+02:00"
            },
            "Created": {
                "Identifier": "Created",
                "Name": "Project Created",
                "Status": "Created",
                "Reference": "feature/Created",
                "Username": "Tom Riedl",
                "Avatar": "none",
                "Weather": 0.9,
                "ImportantRefStatus": "Success",
                "StartedAt": "2017-09-30T02:36:22+02:00"
            },
            "Pending": {
                "Identifier": "Pending",
                "Name": "Project Pending",
                "Status": "Pending",
                "Reference": "feature/Pending",
                "Username": "Charlton Mu",
                "Weather": 0.8,
                "ImportantRefStatus": "Success",
                "StartedAt": "2017-09-30T02:37:22+02:00"
            },
            "Running": {
                "Identifier": "Running",
                "Name": "Project Running",
                "Status": "Running",
                "LastStatus": "Success",
                "Reference": "feature/Running",
                "Username": "Bruce Mattie",
                "Weather": 0.7,
                "ImportantRefStatus": "Success",
                "StartedAt": "Now",
                "EstimatedEnd": "Now+"
            },
            "Canceled": {
                "Identifier": "Canceled",
                "Name": "Project Canceled",
                "Status": "Canceled",
                "Reference": "feature/Canceled",
                "Username": "Leontyne Ray",
                "Weather": 0.6,
                "ImportantRefStatus": "Success",
                "StartedAt": "2017-09-30T03:25:11+02:00",
                "FinishedAt": "2017-09-30T03:26:11+02:00"
            },
            "Failed": {
                "Identifier": "Failed",
                "Name": "Project Failed",
                "Status": "Failed",
                "Reference": "feature/Failed",
                "Username": "Nikki Lenore",
                "Weather": 0.5,
                "ImportantRefStatus": "Success",
                "Avatar": "images/avatars/NikkiLenore.svg",
                "StartedAt": "2017-09-30T03:00:17+02:00",
                "FinishedAt": "2017-09-30T03:01:17+02:00"
            },
            "Unknown": {
                "Identifier": "Unknown",
                "Name": "Project Unknown",
                "Status": "Unknown",
                "Reference": "feature/Unknown",
                "Username": "Nikki Lenore",
                "Weather": 0.5,
                "ImportantRefStatus": "Unknown",
                "Avatar": "images/avatars/NikkiLenore.svg",
                "StartedAt": "2017-09-30T03:00:17+02:00",
                "FinishedAt": "2017-09-30T03:01:17+02:00"
            },
            "SuccessFailed": {
                "Identifier": "SuccessFailed",
                "Name": "Project Success + Failed",
                "Status": "Success",
                "Reference": "feature/Success+Failed",
                "Username": "Michael Loeffl",
                "Avatar": "none",
                "Weather": 0.4,
                "ImportantRefStatus": "Failed",
                "StartedAt": "2017-09-30T02:29:30+02:00",
                "FinishedAt": "2017-09-30T02:30:30+02:00"
            },
            "CreatedFailed": {
                "Identifier": "CreatedFailed",
                "Name": "Project Created + Failed",
                "Status": "Created",
                "Reference": "feature/Created+Failed",
                "Username": "Tom Riedl",
                "Avatar": "none",
                "Weather": 0.3,
                "ImportantRefStatus": "Failed",
                "StartedAt": "2017-09-30T02:36:22+02:00"
            },
            "PendingFailed": {
                "Identifier": "PendingFailed",
                "Name": "Project Pending + Failed",
                "Status": "Pending",
                "Reference": "feature/Pending+Failed",
                "Username": "Charlton Mu",
                "Weather": 0.2,
                "ImportantRefStatus": "Failed",
                "StartedAt": "2017-09-30T02:37:22+02:00"
            },
            "RunningFailed": {
                "Identifier": "RunningFailed",
                "Name": "Project Running + Failed",
                "Status": "Running",
                "LastStatus": "Failed",
                "Reference": "feature/Running+Failed",
                "Username": "Bruce Mattie",
                "Weather": 0.1,
                "ImportantRefStatus": "Failed",
                "StartedAt": "Now",
                "EstimatedEnd": "Now+"
            },
            "CanceledFailed": {
                "Identifier": "CanceledFailed",
                "Name": "Project Canceled + Failed",
                "Status": "Canceled",
                "Reference": "feature/Canceled+Failed",
                "Username": "Leontyne Ray",
                "Weather": 0.0,
                "ImportantRefStatus": "Failed",
                "StartedAt": "2017-09-30T03:25:11+02:00",
                "FinishedAt": "2017-09-30T03:26:11+02:00"
            },
            "FailedFailed": {
                "Identifier": "FailedFailed",
                "Name": "Project Failed + Failed",
                "Status": "Failed",
                "Reference": "feature/Failed+Failed",
                "Username": "Bruce Mattie",
                "Weather": 0.0,
                "ImportantRefStatus": "Failed",
                "Avatar": "images/avatars/BruceMattie.svg",
                "StartedAt": "2017-09-30T03:00:17+02:00",
                "FinishedAt": "2017-09-30T03:01:17+02:00"
            },
            "UnknownFailed": {
                "Identifier": "UnknownFailed",
                "Name": "Project Unknown + Failed",
                "Status": "Unknown",
                "Reference": "feature/Unknown+Failed",
                "Username": "Nikki Lenore",
                "Weather": 0.5,
                "ImportantRefStatus": "Failed",
                "Avatar": "images/avatars/NikkiLenore.svg",
                "StartedAt": "2017-09-30T03:00:17+02:00",
                "FinishedAt": "2017-09-30T03:01:17+02:00"
            }
        }
    },

    Logger: null,
    ConnectorConfig: null,
    DataSink: null,

    Initialize: function()
    {
        for (var key in this.Shared.Projects)
        {
            var project = this.Shared.Projects[key];

            project.Avatar = project.Avatar === "none" ? null : "images/avatars/" + project.Username.replace(" ", "") + ".svg";

            if (project.StartedAt === "Now")
            {
                project.StartedAt = moment().utc().format();
            }
            if (project.EstimatedEnd === "Now+")
            {
                var buildTime = 3;
                project.EstimatedEnd = moment().utc().add(buildTime, "m").format();
            }
        }
    },

    Start: function()
    {
        this.DataSink.Update(this.Shared.Projects, null, this);
    },

    Stop: function()
    {
        // Do nothing
    }
};